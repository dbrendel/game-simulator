# Game simulator

Future purpose of this little tool is to simulate a board game so that different
strategies can be statistically evaluated.

Current purpose is to get some familiarity with the Rust programming language.

# License

GPL-3.0 or later.
