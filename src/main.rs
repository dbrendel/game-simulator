// SPDX-License-Identifier: GPL-3.0-or-later

use std::io;

use rand::Rng;

pub struct Player {
    id: u8,
    pos: i8,
    money: i32,
}

impl Player {
    pub fn new(id: u8) -> Player {
        Player {
            id,
            pos: 0,
            money: 1200,
        }
    }

    pub fn add_pos(&mut self, n: i8) {
        self.pos += n;
    }

    pub fn add_money(&mut self, n: i32) {
        self.money += n;
    }
}

fn print_player_stats(player: &Player) {
    println!("Player: {}", player.id);
    println!("\tPosition: {}", player.pos);
    println!("\tMoney: {}", player.money);
}

// This probably is slow
fn roll_dice() -> u8 {
    let mut rng = rand::thread_rng();
    return rng.gen_range(1..=6);
}

fn main() {
    let mut input = String::new();
    let mut players: Vec<Player> = Vec::new();
    let mut die1_val: u8;
    let mut die2_val: u8;
    let mut die_sum: u8;

    println!("Welcome to Game Simulator!");
    println!();
    println!("How many players?");

    io::stdin().read_line(&mut input).expect("Wrong input!");
    let num_players: u8 = input.trim().parse().unwrap();

    for i in 0..num_players {
        players.push(Player::new(i));
    }

    loop {
        die1_val = roll_dice();
        die2_val = roll_dice();
        die_sum = die1_val + die2_val;
        println!("Moving player 1 by {:2} ({} + {})",
                 die_sum, die1_val, die2_val);

        players[0].add_pos(die_sum as i8);

        // TODO: handle 3 doubles as "go to jail"
        if die1_val != die2_val
        {
            break;
        }
    }

    for player in players {
        print_player_stats(&player);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn new_player_properties() {
        let id = 2;
        let player = Player::new(id);

        assert_eq!(player.id, id);
        assert_eq!(player.pos, 0);
        assert_eq!(player.money, 1200);
    }

    #[test]
    fn player_move() {
        let id = 0;
        let mut player = Player::new(id);

        player.add_pos(3);
        assert_eq!(player.pos, 3);

        player.add_pos(-2);
        assert_eq!(player.pos, 1);
    }

    #[test]
    fn player_money_change() {
        let id = 0;
        let mut player = Player::new(id);

        player.add_money(200);
        assert_eq!(player.money, 1400);

        player.add_money(-1400);
        assert_eq!(player.money, 0);

        player.add_money(-10);
        assert_eq!(player.money, -10);
    }
}
